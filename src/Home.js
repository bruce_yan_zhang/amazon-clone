import React from 'react'
import './Home.css'
import Product from './Product'

function Home() {
    return (
        <div className='home'>
            <div className='home__container'>
                <img className='home__image' src="https://images-fe.ssl-images-amazon.com/images/G/35/digital/video/merch/2020/Other/BRND_MTH20_00000_GWBleedingHero_3000x1200_Final_en-AU_ENG_PVD5583._CB402965731_.jpg"></img>

                <div className="home_row">
                    <Product />
                    <Product />
                </div>

                <div className="home_row">
                    <Product />
                    <Product />
                    <Product />
                </div>

                <div className="home_row">
                    <Product />
                </div>
            </div>
        </div>
    )
}

export default Home
