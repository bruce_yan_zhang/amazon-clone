import React from 'react'
import "./Product.css"
import StarIcon from '@material-ui/icons/Star';

function Product() {
    return (
        <div className="product">
            <div className="product__info">
                <p>The lean Startup: How Constant Innovation Creates Radically Successful Businesses Paperback</p>
                <p className="product__price">
                    <small>$</small>
                    <strong>19.99</strong>
                </p>
                <div className="product__rating">
                    <StarIcon />
                    <StarIcon />
                    <StarIcon />
                    <StarIcon />
                    <StarIcon />
                </div>
            </div>

            <img src="https://m.media-amazon.com/images/I/51BKOXHmv2L._AC_SL520_.jpg" alt="" />
            
            <button>ADD TO BASKET</button>
        </div>
    )
}

export default Product
